package com.rishabh.a20220630_rishabharora_nycschools.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rishabh.a20220630_rishabharora_nycschools.R

/**
 * This is the main activity of the app.
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}