package com.rishabh.a20220630_rishabharora_nycschools.ui.base

import androidx.lifecycle.ViewModel

/**
 * Base View Model class
 */
abstract class BaseViewModel : ViewModel()