package com.rishabh.a20220630_rishabharora_nycschools.util;

/**
 *  Status of a resource that is provided to the UI.
 */
public enum Status {
    LOADING,
    SUCCESS,
    ERROR
}
